package ru.karamyshev.taskmanager.api.service;

import ru.karamyshev.taskmanager.model.Project;
import ru.karamyshev.taskmanager.model.Task;

import java.util.List;

public interface IProjectService {
    void create(String name);

    void create(String name, String description);

    void add(Project project);

    void remove(Project project);

    List<Project> findAll();

    void clear();

    Project findOneByIndex(Integer index);

    Project findOneByName(String name);

    Project updateProjectById(String id, String name, String description);

    Project removeOneByIndex(Integer index);

    Project removeOneByName(String name);

    Project findOneById(String id);

    Project removeOneById(String id);

    Project updateProjectByIndex(Integer index, String name, String description);
}
