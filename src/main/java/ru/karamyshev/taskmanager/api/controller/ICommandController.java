package ru.karamyshev.taskmanager.api.controller;

public interface ICommandController {

     void showVersion();

     void showAbout();

     void showHelp();

     void showCommands();

     void showArguments();

     void exit();

     void showInfo();
}
