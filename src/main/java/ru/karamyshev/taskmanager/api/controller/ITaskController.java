package ru.karamyshev.taskmanager.api.controller;

public interface ITaskController {

    void showTasks();

    void clearTasks();

    void createTasks();

    void showTaskById();

    void showTaskByIndex();

    void showTaskByName();

    void updateTaskById();

    void updateTaskByIndex();

    void removeTaskById();

    void removeTaskByIndex();

    void removeTaskByName();
}
