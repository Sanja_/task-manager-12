package ru.karamyshev.taskmanager.api.repository;

import ru.karamyshev.taskmanager.model.Project;
import java.util.List;

public interface IProjectRepository {

    void add(Project project);

    void remove(Project project);

    List<Project> findAll();

    void clear();

    Project findOneById(String id);

    Project removeOneById(String id);

    Project findOneByIndex(Integer index);

    Project findOneByName(String name);

    Project removeOneByIndex(Integer index);

    Project removeOneByName(String name);
}
