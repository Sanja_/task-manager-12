package ru.karamyshev.taskmanager.controller;

import ru.karamyshev.taskmanager.api.controller.IProjectController;
import ru.karamyshev.taskmanager.api.service.IProjectService;
import ru.karamyshev.taskmanager.model.Project;
import ru.karamyshev.taskmanager.model.Task;
import ru.karamyshev.taskmanager.util.TerminalUtil;

import java.util.List;

public class ProjectController implements IProjectController {

    private IProjectService projectService;

    public ProjectController(final IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void showProject() {
        System.out.println("[PROJECT TASKS]");
        final List<Project> projects = projectService.findAll();
        for (Project project : projects) System.out.println(project);
        System.out.println("[OK]");
    }

    @Override
    public void clearProject() {
        System.out.println("[CLEAR PROJECT]");
        projectService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void createProject() {
        System.out.println("[CREATE PROJECT");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION");
        final String description = TerminalUtil.nextLine();
        projectService.create(name, description);
        System.out.println("[OK]");
    }

    @Override
    public void createProjects() {
        System.out.println("[CREATE PROJECT");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION");
        final String description = TerminalUtil.nextLine();
        projectService.create(name, description);
        System.out.println("[OK]");
    }

    @Override
    public void showProjectById() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(id);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        showProjects(project);
        System.out.println("[OK]");
    }

    @Override
    public void showProjectByIndex() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber();
        final Project project = projectService.findOneByIndex(index);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        showProjects(project);
        System.out.println("[OK]");
    }

    private void showProjects(final Project project) {
        if (project == null) return;
        System.out.println("ID:" + project.getId());
        System.out.println("NAME:" + project.getName());
        System.out.println("DESCRIPTION:" + project.getDescription());
    }

    @Override
    public void showProjectByName() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.findOneByName(name);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        showProjects(project);
        System.out.println("[OK]");
    }

    @Override
    public void updateProjectById() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(id);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdate = projectService.updateProjectById(id,name,description);
        if (projectUpdate == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void updateProjectByIndex() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber();
        final Project Project = projectService.findOneByIndex(index);
        if (Project == null) {
            System.out.println("[FAIL]");
            return ;
        }
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdate = projectService.updateProjectByIndex(index, name, description);
        if (projectUpdate == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void removeProjectById() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER PROJECT ID FOR DELETION:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.removeOneById(id);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void removeProjectByIndex() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER PROJECT INDEX FOR DELETION:");
        final Integer index = TerminalUtil.nextNumber();
        final Project project = projectService.removeOneByIndex(index);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void removeProjectByName() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER PROJECT NAME FOR DELETION:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.removeOneByName(name);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }
}
