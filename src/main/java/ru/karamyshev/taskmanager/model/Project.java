package ru.karamyshev.taskmanager.model;

import java.util.UUID;

public class Project {

    private long id = UUID.randomUUID().getMostSignificantBits();

    private String name = "default name project";

    private String description = "default description project";

    public long getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return id + ": " + name;
    }
}
