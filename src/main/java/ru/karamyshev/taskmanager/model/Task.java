package ru.karamyshev.taskmanager.model;

import java.util.UUID;

public class Task {

    private long id = UUID.randomUUID().getMostSignificantBits();

    private String name = "default name task";

    private String description = "default description task";

    public long getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return id + ": " + name;
    }
}
