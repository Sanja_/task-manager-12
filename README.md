# Информация о проекте.
## Приложение "Task Manager"
Осуществляет вывод информации по введённой команде.

### Команды (Аргументы):
- help (-h) - доступные команды.
- about (-a) - информация о разработчике.
- version (-v) - версия приложения.
- info (-i) - информация о системе.
- exit - закрывает приложение.
- argument (-arg) - все аргументы для запуска через консоль.
- commands (-cmd) - все команды приложения.
- task-create - создаёт новую задачу.
- task-clear - удаляет все задачи.
- task-list - выводит все записанные задачи.
- project-create - создаёт новый проект.
- project-clear - удаляет все проекты.
- project-list - выводит все записанные проекты.


# Стек
- Java 8.
- IntelliJ IDEA.
- Maven 3.

# Аппаратное обеспечение.
Процессор: 
- Intel Core 2 Quad и выше.
- Amd Athlon 64 и выше. 

ОЗУ: 2гб.    

Графический память: 512 Мб.

Переферийные устройства: клавиатура, мышь.          
       
# Программное обеспечение.
- JDK 1.8.
- Windows 7.
- Maven 3.

# Сборка jar файла
``` 
mvn clean package 
```
# Запуск приложения.
 ```
 java -jar target/taskmanager-1.0.0.jar -h -v -a -i -arg -cmd
 ```

![](https://drive.google.com/uc?export=view&id=1m7QCkll66tcSVsT99UfMBNiw8xunk3ua)

### Ввод команд в консоль приложения

![](https://drive.google.com/uc?export=view&id=19Ek7piAn0bylORYvoHS3KFe-dOrs0mlL)

![](https://drive.google.com/uc?export=view&id=1kUmDgAlDw3pMAfjXO_3_8O9PyTWHKRzR)

![](https://drive.google.com/uc?export=view&id=1Uar8PauGaa7h9zWOxAr7_SiCODUbiNc6)

# Разработчики.
**Имя**: Александр Карамышев.

**Телефон:** 8(800)-555-35-35.

**Email**: sanja_19.96@mail.ru.
